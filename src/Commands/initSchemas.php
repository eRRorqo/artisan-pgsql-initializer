<?php

namespace ArtisanPgsqlInitializer\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class initSchemas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pgsql:schemas {schema?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create schemas and run migrations from json file in ./database/schema/example-schemas.json';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $schema_name = $this->argument('schema');
        $schemas = [];
        $dir = 'database/schema/';

        if(!empty($schema_name)) {
            $fileContent = file_get_contents(base_path($dir. $schema_name . '.json'));
            $schemas = json_decode($fileContent, true);
            
            $this->createSchemas($schemas, $schema_name);
            $this->doMigrations($schemas['migrations']);
        } else {
            $files = array_diff(scandir(base_path($dir)), array('..', '.'));
            $migrations = [];
            foreach ($files as $file) {
                if(!is_dir($dir. $file))
                {
                    $fileContent = file_get_contents(base_path($dir. $file));
                    $schemas = json_decode($fileContent, true);
                    $this->createSchemas($schemas, $file);
                    $migrations = array_merge($migrations, $schemas['migrations']);
                }
            }
            $migrations = array_unique($migrations);
            $this->doMigrations($migrations);
            return true;
        }

    }

    private function validSchemas($schemas) {
        $validator = Validator::make($schemas, [
            'schemas' => 'required',
            'migrations' => 'required'

        ],
        [
            'schemas.required' => 'Key "schemas" is missing',
            'migrations.required' => 'Key "migrations" is missing'
        ]);

        if($validator->fails()) {
            foreach ($validator->errors()->all() as $message) {
                $this->line('<fg=red>'.$message.'</>');
            }
            return false;
        }
        return true;
    }

    private function createSchemas($schemas, $file) {
        if(!$this->validSchemas($schemas)) {
            return false;
        } 

        foreach ($schemas['schemas'] as $schema) {
            $query = "CREATE SCHEMA IF NOT EXISTS $schema;";
            if(DB::statement($query)) {
                $this->line('<fg=green>Schema "'. $schema .'" has been created successfully!</>');
            }

        }
        $this->line('');
        $this->line('<fg=green>Schemas from file ' . base_path('database\schema\\'. $file ) . ' were successfully created!</>');
        $this->line('');
      
        return true;
    }

    private function doMigrations($migrations) {
        $this->line('<fg=blue>Folders to migrate:</>');
        foreach ($migrations as $folder_to_migrate) {
            $this->line('<fg=blue>'.$folder_to_migrate.'</>');
        }
        if($this->confirm('Would you like to run all these migrations?')) {
            foreach ($migrations as $folder_to_migrate) {
                $this->line('<fg=yellow>Migrating ' . $folder_to_migrate . '</>');
                Artisan::call('migrate --path='.$folder_to_migrate . ' --force');
            }
            $this->line('<fg=green>Success! Everything is ready now</>');

        }
    }
}
