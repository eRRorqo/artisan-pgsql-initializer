<?php

namespace ArtisanPgsqlInitializer\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class psql extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pgsql:initdb {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create database {name}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dbname = $this->argument('name') ?: config("database.connections.pgsql.database");

        config(["database.connections.pgsql.database" => 'postgres']);

        $query = "CREATE DATABASE $dbname WITH OWNER = postgres ENCODING = 'UTF8' CONNECTION LIMIT = -1;";

        DB::statement($query);
        config(["database.connections.pgsql.database" => $dbname]);
    }
}
