<?php

namespace ArtisanPgsqlInitializer;

use Illuminate\Support\ServiceProvider;

class ArtisanPgsqlInitializerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {   
        $dir = 'database/schema/';
        if(!file_exists(base_path($dir))) {
            mkdir(base_path($dir));
            $file = fopen(base_path($dir. 'example-schemas.json'), 'w');
            $example = '{
                "schemas":[
                    "schema1", 
                    "schema2", 
                    "schema3"
                ],
                "migrations": [
                    "database/migrations/schema1",
                    "database/migrations/schema2",
                    "database/migrations/schema3"       
                ]
            }';
            fwrite($file, $example);
            fclose($file);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
