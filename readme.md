# Artisan Pgsql Initializer

Simple laravel package to init postgresql database and schemas

##### Commands:
- php artisan psql:initdb {name}
- php artisan pgsql:schemas {schemas} - arg is optional

## Installation & usage

#### Installation
```
composer require error/artisan-pgsql-initializer
```

#### Usage
After installation it creates folder ```your-app/database/schema``` with example file
```schemas``` - db schemas to create (names)
```migrations``` - migrate migrations in these folders after creating schemas (folder names in ```your-app/database/migrations/example/...```)

## License

MIT

**Free Software, Hell Yeah!**
